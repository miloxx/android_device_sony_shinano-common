Copyright 2013 - The CyanogenMod Project
Copyright 2017 - The LineageOS Project
Copyright 2017 - The OmniRom Project
================================

This is the Android device configuration for shinano platform.

Shinano devices:
----------------

    Xperia Z2..........................Sirius
    Xperia Z2 Tablet...................Castor
    Xperia Z2 Tablet (Wi-Fi)...........Castor_windy
    Xperia Z3..........................Z3             (Leo)
    Xperia Z3 Compact..................Z3C            (Aries)
    Xperia Z3 Dual.....................Z3Dual
    Xperia Z3 Tablet Compact...........Scorpion
    Xperia Z3 Tablet Compact (Wi-Fi)...Scorpion_windy


Depend on (Sony specific):
--------------------------

    omnirom/android_device_sony_msm8974-common
    omnirom/android_device_sony_common

| Version | Kernel version | Kernel repository                     |
|---------|----------------|---------------------------------------|
| 8.0     | 3.4            | LineageOS/android_kernel_sony_msm8974 |
| 7.1     | 3.4            | LineageOS/android_kernel_sony_msm8974 |
| 6.x     | 3.10           | LineageOS/android_kernel_sony_msm     |
